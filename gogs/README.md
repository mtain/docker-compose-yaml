# 使用docker-compose搭建Gogs

#### 操作过程
```
# 解压分卷
cat mysql.zip.* > mysql.zip && unzip mysql.zip
unzip gogs.zip


# 镜像导入
docker load –i mysql.tar
docker load –i gogs.tar

# 创建数据目录
mkdir -p /gogs/

# 创建docker网络
docker network create --driver=bridge --subnet=172.16.0.0/16 gogs
docker network ls

	
# 后台启动
docker-compose up -d
```

#### 访问使用
浏览器访问
http://ip:3000

数据库配置ip填写
172.16.0.40

注册的第一个用户即为管理员

#### 其它
镜像导出
docker save -o mysql.tar docker.io/mysql:5.7.25
docker save -o gogs.tar docker.io/gogs/gogs:latest


镜像导入
docker load –i mysql.tar
docker load –i gogs.tar