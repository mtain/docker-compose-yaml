# 使用docker-compose搭建Gogs

#### 过程
```
# 解压分卷
cat mysql.zip.* > mysql.zip && unzip mysql.zip

# 镜像导入
docker load –i mysql.tar

# 创建数据目录
mkdir -p /data/mysql
mkdir -p /data/conf.d

	
# 后台启动
docker-compose up -d
```

#### 访问使用
数据库连接
ip:3306





