# docker-compose-yaml

#### 介绍
1. 日常docker-compose搭建环境的常见yaml
2. 自带离线docker镜像文件

例如：使用docker-compose单机快速搭建mysql,owncloud等

#### 软件清单
**gogs：** Git服务器
**minio：** S3对象存储(S3-Object-Storage
**mysql5.7.25：** mysql5.7.25数据库
**nexus3：** Maven私服
**nginx-http：** nginx-http配置
**nginx-https：** nginx-https配置
**owncloud：** 私有云盘

#### 常用镜像
```shell
# postgresql
docker run --name postgres -e POSTGRES_PASSWORD=xxxxx -p 5432:5432 -d postgres:13.1


# consul
docker run -d -p 8500:8500 --restart=always --name=consul consul:latest agent -server -bootstrap -ui -node=1 -client='0.0.0.0'



# elasticsearch
docker run -e ES_JAVA_OPTS="-Xms256m -Xmx256m" -e "discovery.type=single-node" -d -p 9200:9200 -p 9300:9300 --name elasticsearch elasticsearch:7.8.0

```



#### docker-compose使用

进入yaml所在文件夹操作

```
# 前台启动
docker-compose up

# 后台启动
docker-compose up -d

# 查看日志
docker-compose logs

# 停止所有服务
docker-compose stop

# 删除所有服务
docker-compose rm

# 停止并删除所有服务
docker-compose down
```


#### 镜像导入导出
```
# Linux解压zip分卷
cat owncloud.zip.* > owncloud.zip && unzip owncloud.zip

# 镜像导出
docker save -o mysql.tar docker.io/mysql:5.7.25

# 镜像导入
docker load –i mysql.tar
```


